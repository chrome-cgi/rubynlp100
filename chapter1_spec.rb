require_relative 'chapter1'

describe Chapter1 do
  before(:each) do
    @c = Chapter1.new
  end

  it "q00" do
    str = "stressed"
    expect(@c.reverse(str)).to eq "desserts"
  end

  it "q01" do
    str = "パタトクカシーー"
    expect(@c.stepPick(str)).to eq "パトカー"
  end

  it "q02" do
    strs = ["パトカー", "タクシー"]
    expect(@c.mixStrs(strs)).to eq "パタトクカシーー"
  end

  it "q03" do
    str = "Now I need a drink, alcoholic of course, after the heavy lectures involving quantum mechanics."
    expect(@c.lengthList(str)).to eq [3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 8, 9, 7, 9]
  end

  it "q04" do
    str = "Hi He Lied Because Boron Could Not Oxidize Fluorine. New Nations Might Also Sign Peace Security Clause. Arthur King Can."

    elements = {
      "H"=>1, "He"=>2, "Li"=>3, "Be"=>4, "B"=>5, "C"=>6, "N"=>7, "O"=>8, "F"=>9, "Ne"=>10,
      "Na"=>11, "Mi"=>12, "Al"=>13, "Si"=>14, "P"=>15, "S"=>16, "Cl"=>17, "Ar"=>18, "K"=>19, "Ca"=>20
    }
    expect(@c.elementList(str)).to eq elements
  end

  it "q05" do
    str = "I am an NLPer"
    expect(@c.wordNGram(str, 2)).to eq [["I", "am"], ["am", "an"], ["an", "NLPer"]]
    expect(@c.charNGram(str, 2)).to eq [["I", " "], [" ", "a"], ["a", "m"], ["m", " "], [" ", "a"], ["a", "n"], ["n", " "], [" ", "N"], ["N", "L"], ["L", "P"], ["P", "e"], ["e", "r"]]
  end

  it "q06" do
    x = @c.charNGram("paraparaparadise", 2)
    y = @c.charNGram("paragraph", 2)
    expect(@c.unions(x, y)).to eq [["p", "a"], ["a", "r"], ["r", "a"], ["a", "p"], ["a", "d"], ["d", "i"], ["i", "s"], ["s", "e"], ["a", "g"], ["g", "r"], ["p", "h"]]
    expect(@c.intersections(x, y)).to eq [["p", "a"], ["a", "r"], ["r", "a"], ["a", "p"]]
    expect(@c.complements(x, y)).to eq [["a", "d"], ["d", "i"], ["i", "s"], ["s", "e"], ["a", "g"], ["g", "r"], ["p", "h"]]
    expect(x.include?(["s", "e"])).to be true
    expect(y.include?(["s", "e"])).not_to be true
  end

  it "q07" do
    expect(@c.template(12, "気温", 22.4)).to eq "12時の気温は22.4"
  end

  it "q08" do
    str = "The quick brown fox jumps over the lazy dog"
    expect(@c.cipher(@c.cipher(str))).to eq str
  end

  it "q09" do
    str = "I couldn't believe that I could actually understand what I was reading : the phenomenal power of the human mind ."

    typo = @c.typoglycemia(str)
    expect(typo).not_to eq str
    typo.split.each.with_index{|w, i|
      baseWord = str.split[i]
      if w.length <= 4
        expect(w).to eq baseWord
      else
        expect(w[0]).to eq baseWord[0]
        expect(w[-1]).to eq baseWord[-1]
      end
    }
  end
end
